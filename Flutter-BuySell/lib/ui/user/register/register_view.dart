import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutterbuyandsell/config/ps_colors.dart';
import 'package:flutterbuyandsell/config/ps_config.dart';
import 'package:flutterbuyandsell/constant/ps_dimens.dart';
import 'package:flutterbuyandsell/constant/route_paths.dart';
import 'package:flutterbuyandsell/provider/ps_provider_dependencies.dart';
import 'package:flutterbuyandsell/provider/user/user_provider.dart';
import 'package:flutterbuyandsell/repository/user_repository.dart';
import 'package:flutterbuyandsell/ui/common/dialog/error_dialog.dart';
import 'package:flutterbuyandsell/ui/common/dialog/warning_dialog_view.dart';
import 'package:flutterbuyandsell/ui/common/ps_button_widget.dart';
import 'package:flutterbuyandsell/utils/utils.dart';
import 'package:flutterbuyandsell/viewobject/common/ps_value_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:file_picker/file_picker.dart';

class RegisterView extends StatefulWidget {
  const RegisterView(
      {Key key,
      this.animationController,
      this.onRegisterSelected,
      this.goToLoginSelected})
      : super(key: key);
  final AnimationController animationController;
  final Function onRegisterSelected, goToLoginSelected;
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  UserRepository repo1;
  PsValueHolder valueHolder;


  TextEditingController nameController;
  TextEditingController emailController;
  TextEditingController passwordController;
  TextEditingController phoneNoController;
  TextEditingController genderController;
  TextEditingController locationController;
  TextEditingController validIdController;
  TextEditingController companyTypeController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    // nameController.dispose();
    // emailController.dispose();
    // passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(
            parent: animationController,
            curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));

    animationController.forward();

    repo1 = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);

    return SliverToBoxAdapter(
      child: ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          final UserProvider provider =
              UserProvider(repo: repo1, psValueHolder: valueHolder);

          return provider;
        },
        child: Consumer<UserProvider>(builder:
            (BuildContext context, UserProvider provider, Widget child) {
          nameController = TextEditingController(
              text: provider.psValueHolder.userNameToVerify);
          emailController = TextEditingController(
              text: provider.psValueHolder.userEmailToVerify);
          passwordController = TextEditingController(
              text: provider.psValueHolder.userPasswordToVerify);
          phoneNoController = TextEditingController(
              text: provider.psValueHolder.userPhoneNumberToVerify);
          genderController = TextEditingController(
              text: provider.psValueHolder.userGenderToVerify);
          locationController = TextEditingController(
              text: provider.psValueHolder.userLocationToVerify);
          validIdController = TextEditingController(
              text: provider.psValueHolder.userValidIdToVerify);
          companyTypeController = TextEditingController(
              text: provider.psValueHolder.userCompanyTypeToVerify);

          return Stack(
            children: <Widget>[
              SingleChildScrollView(
                  child: AnimatedBuilder(
                      animation: animationController,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          _HeaderIconAndTextWidget(),
                          _TextFieldWidget(
                            provider: provider,
                            nameText: nameController,
                            emailText: emailController,
                            passwordText: passwordController,
                            phoneNoText: phoneNoController,
                            locationText: locationController,
                            validId: validIdController,
                            genderText: genderController,
                            companyType: companyTypeController,
                          ),
                          const SizedBox(
                            height: PsDimens.space8,
                          ),
                          _TermsAndConCheckbox(
                            provider: provider,
                            nameTextEditingController: nameController,
                            emailTextEditingController: emailController,
                            passwordTextEditingController: passwordController,
                            phoneNumberTextEditingController: phoneNoController,
                            validIdTextEditingController: validIdController,
                            locationTextEditingController: locationController,
                          ),
                          const SizedBox(
                            height: PsDimens.space8,
                          ),
                          _SignInButtonWidget(
                            provider: provider,
                            nameTextEditingController: nameController,
                            emailTextEditingController: emailController,
                            passwordTextEditingController: passwordController,
                            onRegisterSelected: widget.onRegisterSelected,
                          ),
                          const SizedBox(
                            height: PsDimens.space16,
                          ),
                          _TextWidget(
                            goToLoginSelected: widget.goToLoginSelected,
                          ),
                          const SizedBox(
                            height: PsDimens.space64,
                          ),
                        ],
                      ),
                      builder: (BuildContext context, Widget child) {
                        return FadeTransition(
                            opacity: animation,
                            child: Transform(
                                transform: Matrix4.translationValues(
                                    0.0, 100 * (1.0 - animation.value), 0.0),
                                child: child));
                      }))
            ],
          );
        }),
      ),
    );
  }
}

class _TermsAndConCheckbox extends StatefulWidget {
  const _TermsAndConCheckbox({
    @required this.provider,
    @required this.nameTextEditingController,
    @required this.emailTextEditingController,
    @required this.passwordTextEditingController,
    @required this.phoneNumberTextEditingController,
    @required this.genderTextEditingController,
    @required this.locationTextEditingController,
    @required this.validIdTextEditingController,
    @required this.companyTypeTextEditingController,
  });

  final UserProvider provider;
  final TextEditingController nameTextEditingController,
      emailTextEditingController,
      passwordTextEditingController, phoneNumberTextEditingController, genderTextEditingController,locationTextEditingController, validIdTextEditingController, companyTypeTextEditingController;
  @override
  __TermsAndConCheckboxState createState() => __TermsAndConCheckboxState();
}

class __TermsAndConCheckboxState extends State<_TermsAndConCheckbox> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const SizedBox(
          width: PsDimens.space20,
        ),
        Checkbox(
          activeColor: PsColors.mainColor,
          value: widget.provider.isCheckBoxSelect,
          onChanged: (bool value) {
            setState(() {
              updateCheckBox(
                  widget.provider.isCheckBoxSelect,
                  context,
                  widget.provider,
                  widget.nameTextEditingController,
                  widget.emailTextEditingController,
                  widget.passwordTextEditingController,
                  widget.phoneNumberTextEditingController,
                  widget.genderTextEditingController,
                  widget.locationTextEditingController,
                  widget.validIdTextEditingController,
                  widget.companyTypeTextEditingController
              );
            });
          },
        ),
        Expanded(
          child: InkWell(
            child: Text(
              Utils.getString(context, 'login__agree_privacy'),
              style: Theme.of(context).textTheme.bodyText1,
            ),
            onTap: () {
              setState(() {
                updateCheckBox(
                    widget.provider.isCheckBoxSelect,
                    context,
                    widget.provider,
                    widget.nameTextEditingController,
                    widget.emailTextEditingController,
                    widget.passwordTextEditingController,
                    widget.phoneNumberTextEditingController,
                    widget.genderTextEditingController,
                    widget.locationTextEditingController,
                    widget.validIdTextEditingController,
                    widget.companyTypeTextEditingController
                );
              });
            },
          ),
        ),
      ],
    );
  }
}

void updateCheckBox(
    bool isCheckBoxSelect,
    BuildContext context,
    UserProvider provider,
    TextEditingController nameTextEditingController,
    TextEditingController emailTextEditingController,
    TextEditingController passwordTextEditingController,
    TextEditingController phoneNumberTextEditingController,
    TextEditingController genderTextEditingController,
    TextEditingController locationTextEditingController,
    TextEditingController validIdTextEditingController,
    TextEditingController companyTypeTextEditingController
    ) {
  if (isCheckBoxSelect) {
    provider.isCheckBoxSelect = false;
  } else {
    provider.isCheckBoxSelect = true;
    //it is for holder
    provider.psValueHolder.userNameToVerify = nameTextEditingController.text;
    provider.psValueHolder.userEmailToVerify = emailTextEditingController.text;
    provider.psValueHolder.userPasswordToVerify =
        passwordTextEditingController.text;
    provider.psValueHolder.userPhoneNumberToVerify =
        phoneNumberTextEditingController.text;
    provider.psValueHolder.userLocationToVerify =
        locationTextEditingController.text;
    provider.psValueHolder.userGenderToVerify =
        genderTextEditingController.text;
    provider.psValueHolder.userValidIdToVerify =
        validIdTextEditingController.text;
    provider.psValueHolder.userCompanyTypeToVerify =
        companyTypeTextEditingController.text;
    Navigator.pushNamed(context, RoutePaths.privacyPolicy, arguments: 1);
  }
}

void updateValidID(
    BuildContext context,
    UserProvider provider,
    TextEditingController nameTextEditingController,
    TextEditingController emailTextEditingController,
    TextEditingController passwordTextEditingController,
    TextEditingController phoneNumberTextEditingController,
    TextEditingController locationTextEditingController,
    ) {
    //it is for holder
    provider.psValueHolder.userNameToVerify = nameTextEditingController.text;
    provider.psValueHolder.userEmailToVerify = emailTextEditingController.text;
    provider.psValueHolder.userPasswordToVerify =
        passwordTextEditingController.text;
    provider.psValueHolder.userPhoneNumberToVerify =
        phoneNumberTextEditingController.text;
    provider.psValueHolder.userLocationToVerify =
        locationTextEditingController.text;
    // provider.psValueHolder.userGenderToVerify =
    //     genderTextEditingController.text;
    // provider.psValueHolder.userValidIdToVerify =
    //     validIdTextEditingController.text;
    // provider.psValueHolder.userCompanyTypeToVerify =
    //     companyTypeTextEditingController.text;
    Navigator.popAndPushNamed(context,'/user_register_container');

}

class _TextWidget extends StatefulWidget {
  const _TextWidget({this.goToLoginSelected});
  final Function goToLoginSelected;
  @override
  __TextWidgetState createState() => __TextWidgetState();
}

class __TextWidgetState extends State<_TextWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        child: Ink(
          color: PsColors.backgroundColor,
          child: Text(
            Utils.getString(context, 'register__login'),
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .copyWith(color: PsColors.mainColor),
          ),
        ),
      ),
      onTap: () {
        if (widget.goToLoginSelected != null) {
          widget.goToLoginSelected();
        } else {
          Navigator.pushReplacementNamed(
            context,
            RoutePaths.login_container,
          );
        }
      },
    );
  }
}

class _TextFieldWidget extends StatefulWidget {
const _TextFieldWidget({
    @required this.nameText,
    @required this.emailText,
    @required this.passwordText,
    @required this.phoneNoText,
    @required this.genderText,
    @required this.locationText,
    @required this.validId,
    @required this.companyType,
    @required this.provider,

  });

  final UserProvider provider;
  final TextEditingController nameText, emailText, passwordText, phoneNoText, genderText, locationText, validId, companyType;
  @override
  __TextFieldWidgetState createState() => __TextFieldWidgetState();
}

class __TextFieldWidgetState extends State<_TextFieldWidget> {

  List<String> _companyType = [
    "None",
    "Recycle",
    "Cherity",
  ];
 // ItemLocationProvider _itemLocationProvider;
 // _itemLocationProvider
  List<String> _genderList = [
    "Male",
    "Female",
  ];

  //UserProvider provider;
  String _currentCompanySelectedValue;
  String _currentGenderSelectedValue;

  @override
  Widget build(BuildContext context) {
    const EdgeInsets _marginEdgeInsetWidget = EdgeInsets.only(
        left: PsDimens.space16,
        right: PsDimens.space16,
        top: PsDimens.space4,
        bottom: PsDimens.space4);

    const Widget _dividerWidget = Divider(
      height: PsDimens.space1,
    );
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 0.3,
      margin: const EdgeInsets.only(
          left: PsDimens.space32, right: PsDimens.space32),
      child: Column(
        children: <Widget>[
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.nameText,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__user_name'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.people,
                      color: Theme.of(context).iconTheme.color)),
            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.emailText,
              style: Theme.of(context).textTheme.button.copyWith(),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__email'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.email,
                      color: Theme.of(context).iconTheme.color)),
            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.passwordText,
              obscureText: true,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__password'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.lock,
                      color: Theme.of(context).iconTheme.color)),
              // keyboardType: TextInputType.number,
            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.phoneNoText,
              obscureText: false,
              maxLength: 15,
              keyboardType: TextInputType.number,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  counterText: '',
                  hintText: Utils.getString(context, 'register__phoneNo'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.phone_android,
                      color: Theme.of(context).iconTheme.color)),
              // keyboardType: TextInputType.number,
            ),
          ),
          _dividerWidget,
          Padding(
            padding: const EdgeInsets.only(left: 15.0,right: 15.0),
            child: FormField<String>(
              builder: (FormFieldState<String> state) {
                return InputDecorator(
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(Icons.accessibility,
                          color: Theme.of(context).iconTheme.color)),
                  isEmpty: _currentGenderSelectedValue == '',
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      value: _currentGenderSelectedValue,
                      isDense: true,
                      hint: Text(Utils.getString(context, 'register__gender'), style: Theme.of(context)
                          .textTheme
                          .button
                          .copyWith(color: PsColors.textPrimaryLightColor),),
                      onChanged: (String newValue) {
                        setState(() {
                          _currentGenderSelectedValue = newValue;
                          state.didChange(newValue);
                        });
                      },
                      items: _genderList.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontWeight: FontWeight.w100,),),
                        );
                      }).toList(),
                    ),
                  ),
                );
              },
            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.locationText,
              obscureText: false,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__location'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.location_on,
                      color: Theme.of(context).iconTheme.color)),
              // keyboardType: TextInputType.number,
            ),
          ),
          _dividerWidget,
          InkWell(
            child: Container(
              margin: _marginEdgeInsetWidget,
              child: TextField(
                enabled: false,
                controller: widget.validId,
                obscureText: false,
                style: Theme.of(context).textTheme.button.copyWith(),
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: Utils.getString(context, 'register__validId'),
                    hintStyle: Theme.of(context)
                        .textTheme
                        .button
                        .copyWith(color: PsColors.textPrimaryLightColor),
                    icon: Icon(Icons.upload_file,
                        color: Theme.of(context).iconTheme.color)),
                // keyboardType: TextInputType.number,
              ),
            ),
            onTap: () async {
              if (await Utils.checkInternetConnectivity()) {
                final FilePickerResult result = await FilePicker.platform.pickFiles();

                if(result != null) {
                    PlatformFile file = result.files.first;

                      print(file.name.toString());
                      widget.provider.psValueHolder.userValidIdToVerify = file.name.toString();

                      print(file.name);
                      print(file.bytes);
                      print(file.size);
                      print(file.extension);
                      print(file.path);
                    setState(() {
                      updateValidID(
                          context,
                          widget.provider,
                          widget.nameText,
                          widget.emailText,
                          widget.passwordText,
                          widget.phoneNoText,
                          widget.locationText,
                      );

                    });

                } else {
                  // User canceled the picker
                }
              } else {
                showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) {
                      return ErrorDialog(
                        message:
                        Utils.getString(context, 'error_dialog__no_internet'),
                      );
                    });
              }
            },
          ),
          _dividerWidget,
          Padding(
            padding: const EdgeInsets.only(left: 15.0,right: 15.0),
            child: FormField<String>(
              builder: (FormFieldState<String> state) {
                return InputDecorator(
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(Icons.merge_type_sharp,
                          color: Theme.of(context).iconTheme.color)),
                  isEmpty: _currentCompanySelectedValue == '',
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      value: _currentCompanySelectedValue,
                      isDense: true,
                      hint: Text(Utils.getString(context, 'register__companyType'), style: Theme.of(context)
                          .textTheme
                          .button
                          .copyWith(color: PsColors.textPrimaryLightColor),),
                      onChanged: (String newValue) {
                        setState(() {
                          _currentCompanySelectedValue = newValue;
                          state.didChange(newValue);
                        });
                      },
                      items: _companyType.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontWeight: FontWeight.w100,),),
                        );
                      }).toList(),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class _HeaderIconAndTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(
          height: PsDimens.space32,
        ),
        Container(
          width: 100,
          height: 100,
          child: Image.asset(
            'assets/images/flutter_buy_and_sell_logo.png',
          ),
        ),
        // const SizedBox(
        //   height: PsDimens.space8,
        // ),
        // Text(
        //   Utils.getString(context, ''),  //'app_name'
        //   style: Theme.of(context).textTheme.headline6.copyWith(
        //         fontWeight: FontWeight.bold,
        //         color: PsColors.mainColor,
        //       ),
        // ),
        // const SizedBox(
        //   height: PsDimens.space52,
        // ),
      ],
    );
  }
}

class _SignInButtonWidget extends StatefulWidget {
  const _SignInButtonWidget(
      {@required this.provider,
      @required this.nameTextEditingController,
      @required this.emailTextEditingController,
      @required this.passwordTextEditingController,
      this.onRegisterSelected});
  final UserProvider provider;
  final Function onRegisterSelected;
  final TextEditingController nameTextEditingController,
      emailTextEditingController,
      passwordTextEditingController;

  @override
  __SignInButtonWidgetState createState() => __SignInButtonWidgetState();
}

dynamic callWarningDialog(BuildContext context, String text) {
  showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return WarningDialog(
          message: Utils.getString(context, text),
          onPressed: (){},
        );
      });
}

class __SignInButtonWidgetState extends State<_SignInButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
          left: PsDimens.space32, right: PsDimens.space32),
      child: PSButtonWidget(
        hasShadow: false,
        width: double.infinity,
        titleText: Utils.getString(context, 'register__register'),
        onPressed: () async {
          if (widget.nameTextEditingController.text.isEmpty) {
            callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__input_name'));
          } else if (widget.emailTextEditingController.text.isEmpty) {
            callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__input_email'));
          } else if (widget.passwordTextEditingController.text.isEmpty) {
            callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__input_password'));
          } else {
            if(Utils.checkEmailFormat(widget.emailTextEditingController.text.trim())){
            await widget.provider.signUpWithEmailId(
                context,
                widget.onRegisterSelected,
                widget.nameTextEditingController.text,
                widget.emailTextEditingController.text.trim(),
                widget.passwordTextEditingController.text);
            }else{
              callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__email_format'));
            }
          }
        },
      ),
    );
  }
}
