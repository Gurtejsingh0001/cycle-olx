import 'package:flutter/material.dart';
import 'package:flutterbuyandsell/config/ps_colors.dart';

class DonationScreen extends StatefulWidget {
  const DonationScreen({Key key}) : super(key: key);

  @override
  _DonationScreenState createState() => _DonationScreenState();
}

class _DonationScreenState extends State<DonationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PsColors.mainColor,
        elevation: 0,
      ),
    );
  }
}
